extern crate emflib;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);
    let x = emflib::call_zcashcli_command();
    //println!("{:?}", x.stdout.expect("Failed to open stdout, for echo"));
    println!("{:?}", String::from_utf8(x.stdout).unwrap().trim());
    println!("{:?}", emflib::MemoField::new());
}
