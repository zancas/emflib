/// Extract, validate, and parse Encrypted Memo Field (EMF) contents.
///
/// The EMF is a 512 byte blob packed inside shielded transactions.  As such
/// it has the privacy/security properties provided by the zcash encryption
/// protocol.
///
/// This library provides the following functionality:
///
/// Invoke a zcash-cli rpc command to produce a string representing the
/// decrypted contents of a shielded transaction (NOTE: different levels of
/// privilege are possible).
///
/// Convert the produced string into a Rust type using the serge-json
///
/// Interpret the 
///
/// Validate that such an extracted JSON is a valid ZcCliTransaction
/// instance.

extern crate serde_json;
use std::process::{Command, Stdio, Output};

pub struct Amount { value: u64 } // 0 <= value <= 21000000000000 
/// I am using this:
/// https://zcash-rpc.github.io/z_listreceivedbyaddress.html
/// as my template for this:
pub struct ZcTransaction {
    txid:     String,
    amount:   Amount, 
    memo:     MemoField,
    change:   bool,
    outindex: u64    
}

#[derive(Debug)]
pub struct MemoField { }
impl MemoField {
    pub fn new() -> MemoField {
        MemoField {}
    }
}
   
pub fn deserialize_transaction(serial_tx: &str) {
    MemoField::new();
}

pub fn call_zcashcli_command() -> Output {
    println!("call_zcashcli_command");
    let result = Command::new("echo") // FIX hardcoded path
                 .arg("Hello World!") // replace 'Hello World' with appropriate
                 // args to the zcash-cli to get the transaction.
                 .stdout(Stdio::piped())
                 .spawn()
                 .expect("failed to start echo process");
    let output = result.wait_with_output().expect("Failed to wait on echo");
    // But wait, what's an "output"...
    println!{"{}", output.status};
    if !output.status.success() { panic!("Oh no!") };
    output 
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(deserialize_transaction("Hello"), ());
    }
}
